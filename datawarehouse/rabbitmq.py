"""RabbitMQ messaging implementation."""
import json
import threading

from cki_lib import messagequeue
from cki_lib.logger import get_logger
from django.db import connection
import pika

from datawarehouse import models

LOGGER = get_logger(__name__)
MESSAGE_PENDING_LOCK = threading.Lock()


class MessageQueue:
    """Messages Queue."""

    def __init__(self, host, port, user, password, keepalive_s=0):
        # pylint: disable=too-many-arguments
        """Init."""
        self.queue = messagequeue.MessageQueue(host=host, port=port, user=user,
                                               password=password, keepalive_s=keepalive_s)

    def send(self):
        """Send messages in the queue."""
        with MESSAGE_PENDING_LOCK:
            for message in models.MessagePending.objects.all():
                data = json.loads(message.body)
                try:
                    self.queue.send_message(data=data, queue_name='', exchange=message.exchange)
                except pika.exceptions.AMQPError:
                    LOGGER.exception('Error sending message, will be retried')
                    break
                else:
                    message.delete()

    def add(self, data, exchange):
        """Add message to the outgoing queue."""
        self.bulk_add([(data, exchange)])

    @staticmethod
    def bulk_add(messages_list):
        """
        Bulk add a list of messages to the outgoing queue.

        Parameters:
        messages_list:  a list of messages with the format (data, exchange).
        """
        msgs = [
            models.MessagePending(
                body=json.dumps(data),
                exchange=exchange
            ) for data, exchange in messages_list
        ]
        models.MessagePending.objects.bulk_create(msgs)

    def send_thread(self):
        """Call .send() and close the db connection afterwards."""
        try:
            self.send()
        finally:
            connection.close()
