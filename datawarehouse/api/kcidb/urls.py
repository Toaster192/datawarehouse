"""Urls file."""
from django.urls import path

from . import views

urlpatterns = [
    path('submit', views.Submit.as_view()),
    path('revisions',
         views.RevisionList.as_view(), name='kcidb.revisions.list'),
    path('revisions/reports/missing',
         views.RevisionReportMissing.as_view(), name='kcidb.revisions.reports.missing'),
    path('revisions/<str:id>',
         views.RevisionGet.as_view(), name='kcidb.revisions.get'),
    path('revisions/<str:id>/builds',
         views.BuildList.as_view(), name='kcidb.builds.list'),
    path('revisions/<str:id>/issues',
         views.RevisionIssueOccurrence.as_view(), name='kcidb.revisions.issues'),
    path('revisions/<str:id>/issues/<int:issue_id>',
         views.RevisionIssueOccurrence.as_view(), name='kcidb.revisions.issues'),
    path('revisions/<str:id>/reports',
         views.RevisionReport.as_view(), name='kcidb.revisions.reports'),
    path('revisions/<str:id>/reports/<str:report_id>',
         views.RevisionReport.as_view(), name='kcidb.revisions.reports'),
    path('builds/<str:id>',
         views.BuildGet.as_view(), name='kcidb.builds.get'),
    path('builds/<str:id>/tests',
         views.TestList.as_view(), name='kcidb.tests.list'),
    path('builds/<str:id>/issues',
         views.BuildIssueOccurrence.as_view(), name='kcidb.builds.issues'),
    path('builds/<str:id>/issues/<int:issue_id>',
         views.BuildIssueOccurrence.as_view(), name='kcidb.builds.issues'),
    path('tests/<str:id>',
         views.TestGet.as_view(), name='kcidb.tests.get'),
    path('tests/<str:id>/issues',
         views.TestIssueOccurrence.as_view(), name='kcidb.tests.issues'),
    path('tests/<str:id>/issues/<int:issue_id>',
         views.TestIssueOccurrence.as_view(), name='kcidb.tests.issues'),
]
