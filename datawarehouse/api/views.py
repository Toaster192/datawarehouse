"""API Views."""
from cki_lib.logger import get_logger
from cki_lib.misc import strtobool
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from datawarehouse import models
from datawarehouse import pagination
from datawarehouse import serializers

LOGGER = get_logger(__name__)


class Issue(APIView):
    """Endpoint for Issue handling."""

    queryset = models.Issue.objects.none()

    def get(self, request, issue_id=None):
        """GET request."""
        if issue_id:
            return self._get(request, issue_id)

        return self._list(request)

    def _get(self, request, issue_id):
        """Get a single Issue."""
        try:
            issue = models.Issue.objects.get(id=issue_id)
        except models.Issue.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serialized_issue = serializers.IssueSerializer(issue).data
        return Response(serialized_issue)

    def _list(self, request):
        """Get a list of Issues. Optionally filter by resolved status."""
        resolved = request.GET.get('resolved')

        issues = models.Issue.objects.all()

        if resolved:
            issues = issues.filter(resolved=strtobool(resolved))

        paginator = pagination.DatawarehousePagination()
        paginated_issues = paginator.paginate_queryset(issues, request)
        serialized_issues = serializers.IssueSerializer(paginated_issues, many=True).data

        context = {
            'issues': serialized_issues,
        }

        return paginator.get_paginated_response(context)


class Pipeline(APIView):
    """Get pipeline data."""

    queryset = models.Pipeline.objects.none()

    def get(self, request, pipeline_id):
        """Get information of a pipeline."""
        try:
            pipe = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = serializers.PipelineSerializer

        return Response(serializer(pipe, context={'request': request}).data)


class TestSingle(APIView):
    """Endpoint for handling single test."""

    queryset = models.Test.objects.none()

    def get(self, request, test_id):
        """Return a single test."""
        try:
            test = models.Test.objects.get(id=test_id)
        except models.Test.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serialized_test = serializers.TestSerializer(test).data

        return Response(serialized_test)


class TestList(APIView):
    """Endpoint for handling many tests."""

    queryset = models.Test.objects.none()

    def get(self, request):
        """Return a list of all the tests."""
        tests = models.Test.objects.all()

        paginator = pagination.DatawarehousePagination()
        paginated_tests = paginator.paginate_queryset(tests, request)

        serialized_tests = serializers.TestSerializer(paginated_tests, many=True).data

        context = {
            'tests': serialized_tests,
        }

        return paginator.get_paginated_response(context)


class IssueRegex(APIView):
    """IssueRegex class."""

    queryset = models.IssueRegex.objects.none()

    def get(self, request):
        """Get IssueRegexes."""
        issue_regexes = models.IssueRegex.objects.all()

        paginator = pagination.DatawarehousePagination()
        paginated_issue_regexes = paginator.paginate_queryset(issue_regexes, request)

        serialized_issue_regexes = serializers.IssueRegexSerializer(paginated_issue_regexes, many=True).data

        context = {
            'issue_regexes': serialized_issue_regexes
        }

        return paginator.get_paginated_response(context)
