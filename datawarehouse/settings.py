"""Django settings for datawarehouse project."""
import os
from socket import gethostbyname
from socket import gethostname
from urllib.parse import urlparse

from cki_lib import misc
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

# URL of the instance
DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL', '').rstrip('/')

#################################################################################
#                                                                               #
# Django configurations                                                         #
#                                                                               #
#################################################################################

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SECRET_KEY = os.getenv('SECRET_KEY')

IS_PRODUCTION = misc.is_production()
CSRF_COOKIE_SECURE = IS_PRODUCTION
CORS_ORIGIN_ALLOW_ALL = IS_PRODUCTION
DEBUG = not IS_PRODUCTION
SESSION_COOKIE_SECURE = IS_PRODUCTION

REQUESTS_MAX_WORKERS = os.environ.get("REQUESTS_MAX_WORKERS", 10)
REQUESTS_MAX_RETRIES = os.environ.get("REQUESTS_MAX_RETRIES", 3)
REQUESTS_TIMEOUT_S = os.environ.get("REQUESTS_TIMEOUT_S", 60)
REQUESTS_TIME_BETWEEN_RETRIES_S = os.environ.get("REQUESTS_TIME_BETWEEN_RETRIES_S", 2)  # noqa

ALLOWED_HOSTS = [
    urlparse(os.getenv('DATAWAREHOUSE_URL', 'http://*')).netloc,
    gethostname(),
    gethostbyname(gethostname()),
]

# Allow configuring extra urls where the application can be accessed from.
ALLOWED_HOSTS.extend(
    [
        urlparse(url).netloc
        for url in os.getenv('DATAWAREHOUSE_URL_EXTRAS', '').split()
    ]
)

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'datawarehouse',
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    'debug_toolbar',
    'django_extensions',
    'django_prometheus',
]

MIDDLEWARE = [
    'django_prometheus.middleware.PrometheusBeforeMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django_prometheus.middleware.PrometheusAfterMiddleware',
]

ROOT_URLCONF = 'datawarehouse.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'datawarehouse.context_processors.settings_values',
            ],
        },
    },
]

WSGI_APPLICATION = 'datawarehouse.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.postgresql',
        'NAME': 'datawarehouse',
        'USER': 'datawarehouse',
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': 5432,
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',  # noqa
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = '/code/static'

MEDIA_PATH = '/staticfiles/'
MEDIA_URL = f'{DATAWAREHOUSE_URL}{MEDIA_PATH}'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'staticfiles')

LOGIN_REDIRECT_URL = '/dashboard'
LOGOUT_REDIRECT_URL = '/dashboard'

#################################################################################
#                                                                               #
# Sentry configuration                                                          #
#                                                                               #
#################################################################################
misc.sentry_init(sentry_sdk, integrations=[DjangoIntegration()])

#################################################################################
#                                                                               #
# Debug toolbar configuration                                                   #
#                                                                               #
#################################################################################


def show_toolbar(request):
    """Show the toolbar when requested with the debug parameter."""
    return (
        request.headers.get('x-requested-with') != 'XMLHttpRequest' and
        (request.GET.get('debug') or request.path.startswith('/__debug__/'))
    )


DEBUG_TOOLBAR_CONFIG = {'SHOW_TOOLBAR_CALLBACK': 'datawarehouse.settings.show_toolbar'}

#################################################################################
#                                                                               #
# REST Framework configuration                                                  #
#                                                                               #
#################################################################################

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 30,
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'datawarehouse.api.permissions.DjangoModelPermissionOrReadOnly',
    ]
}

#################################################################################
#                                                                               #
# Shell Plus configuration                                                      #
#                                                                               #
#################################################################################

SHELL_PLUS_PRINT_SQL_TRUNCATE = 10000
SHELL_PLUS_PRE_IMPORTS = (
    ('datawarehouse', 'models'),
)


#################################################################################
#                                                                               #
# RabbitMQ messaging server configuration                                       #
#                                                                               #
#################################################################################

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', '').rstrip('/')
RABBITMQ_PORT = misc.get_env_int('RABBITMQ_PORT', 5672)
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD', 'guest')
RABBITMQ_EXCHANGE_KCIDB_NOTIFICATIONS = os.environ.get('DATAWAREHOUSE_EXCHANGE_KCIDB',
                                                       'cki.exchange.datawarehouse.kcidb')
RABBITMQ_KEEPALIVE_S = misc.get_env_int('RABBITMQ_KEEPALIVE_S', 10)
RABBITMQ_CONFIGURED = bool(RABBITMQ_HOST)

#################################################################################
#                                                                               #
# Celery configuration                                                          #
# https://www.distributedpython.com/2018/07/03/simple-celery-setup/             #
#                                                                               #
#################################################################################

CELERY_FILESYSTEM_PATH = os.environ['CELERY_FILESYSTEM_PATH']
CELERY_BROKER_URL = "filesystem://"
CELERY_BROKER_TRANSPORT_OPTIONS = {
    'data_folder_out': CELERY_FILESYSTEM_PATH,
    'data_folder_in': CELERY_FILESYSTEM_PATH,
    'store_processed': False,
}

#################################################################################
#                                                                               #
# Timers configuration                                                          #
#                                                                               #
#################################################################################

# Timer used to send MessagePending objects in bulk
TIMER_MSG_QUEUE_SEND_PERIOD_S = misc.get_env_int('TIMER_MSG_QUEUE_SEND_PERIOD_S', 10)
# Timer used to send the retriage notification after an IssueRegex was modified
TIMER_RETRIAGE_PERIOD_S = misc.get_env_int('TIMER_RETRIAGE_PERIOD_S', 60*5)

#################################################################################
#                                                                               #
# Email sending configuration                                                   #
#                                                                               #
#################################################################################

# SMTP server to use for sending emails
EMAIL_HOST = os.environ.get('EMAIL_HOST', 'localhost')
# Default sender for the emails
DEFAULT_FROM_EMAIL = os.environ.get('EMAIL_FROM', 'Datawarehouse <admin@datawarehouse>')

#################################################################################
#                                                                               #
# Logging configuration                                                         #
#                                                                               #
#################################################################################

LOGS_PATH = os.environ['LOGS_PATH']
LOGGING = {
    'version': 1,
    'formatters': {
        'requests': {
            'format': '{asctime} - {name} {message}',
            'style': '{',
        },
        'cki': {
            'format': '{asctime} - [{levelname}] - {name} - {message}',
            'style': '{',
        }
    },
    'handlers': {
        'requests': {
            'level': 'INFO',
            'formatter': 'requests',
            'class': 'logging.FileHandler',
            'filename': f'{LOGS_PATH}/django-{gethostname()}-requests.log',
        },
        'cki': {
            'level': os.environ.get('CKI_LOGGING_LEVEL', 'INFO'),
            'class': 'logging.FileHandler',
            'filename': f'{LOGS_PATH}/django-{gethostname()}-cki.log',
            'formatter': 'cki'
        }
    },
    'loggers': {
        # Production server
        'django.request': {
            'handlers': ['requests'],
            'propagate': False,
        },
        # Development server
        'django.server': {
            'handlers': ['requests'],
            'propagate': False,
        },
        'cki': {
            'handlers': ['cki'],
            'propagate': True,
        },
    }
}

#################################################################################
#                                                                               #
# Feature flags and custom configurations                                       #
#                                                                               #
#################################################################################

# Allow submitting pipelines tagged as debug, such as retriggers.
FF_ALLOW_DEBUG_PIPELINES = misc.get_env_bool('FF_ALLOW_DEBUG_PIPELINES', False)

# Enable signup
FF_SIGNUP_ENABLED = misc.get_env_bool('FF_SIGNUP_ENABLED', False)

# URL to the Privacy Policy document. Displayed on the footer of all pages.
PRIVACY_POLICY_URL = os.environ.get('PRIVACY_POLICY_URL')

BEAKER_URL = os.environ.get('BEAKER_URL', '').rstrip('/')
