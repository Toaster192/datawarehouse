"""Context processors."""
from django.conf import settings


def settings_values(_):
    """Add values from the settings module into the templates context."""
    return {
        'PRIVACY_POLICY_URL': settings.PRIVACY_POLICY_URL,
        'FF_SIGNUP_ENABLED': settings.FF_SIGNUP_ENABLED,
    }
