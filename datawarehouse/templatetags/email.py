"""Email templatetags."""
from email import header

from django import template

register = template.Library()


@register.filter
def decode_header(arg):
    """Decode and stringify email header."""
    decoded = header.decode_header(arg)
    return header.make_header(decoded)
