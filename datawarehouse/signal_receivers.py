"""Receiver functions for signals."""
from django import dispatch
from django.conf import settings
from django.db.models.signals import post_save
from django.utils import timezone

from datawarehouse import models
from datawarehouse import scripts
from datawarehouse import signals
from datawarehouse import utils
from datawarehouse.api.kcidb import serializers as kcidb_serializers


@dispatch.receiver(signals.kcidb_object)
def send_kcidb_object_message(**kwargs):
    """Send new kcidb object message."""
    serializers = {
        'revision': kcidb_serializers.KCIDBRevisionSerializer,
        'build': kcidb_serializers.KCIDBBuildSerializer,
        'test': kcidb_serializers.KCIDBTestSerializer,
    }

    status = kwargs['status']
    object_type = kwargs['object_type']
    objects = kwargs['objects']

    messages = [{
        'timestamp': timezone.now().isoformat(),
        'status': status,
        'object_type': object_type,
        'object': serializers[object_type](object_instance).data,
        'id': object_instance.id,
        'iid': object_instance.iid,
    } for object_instance in objects]
    utils.send_kcidb_notification(messages)


@dispatch.receiver(post_save, sender=models.IssueRegex)
def send_kcidb_objects_retriage(**_):
    """Send kcidb objects for retriage after regexes are added / modified."""
    if settings.RABBITMQ_CONFIGURED:
        scripts.TIMER_RETRIAGE.start()
