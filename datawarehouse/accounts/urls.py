"""Account urls."""
from django.conf import settings
from django.urls import include
from django.urls import path

from datawarehouse.accounts import views

urlpatterns = [
    path('user', views.user_details, name='account.user.details'),
    path('user/delete', views.user_delete, name='account.user.delete'),
    path('', include('django.contrib.auth.urls')),
]

if settings.FF_SIGNUP_ENABLED:
    urlpatterns.extend([
        path('signup', views.user_signup, name='account.signup'),
        path('signup/done', views.user_signup_done, name='account.signup.done'),
        path('signup/complete/<uidb64>/<token>', views.user_signup_complete, name='account.signup.complete'),
    ])
