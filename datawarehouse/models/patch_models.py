# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Patch models file."""
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

# Patches stuff
from datawarehouse.utils import parse_patches_from_urls

from . import pipeline_models


class PatchManager(models.Manager):
    """Natural key for Test."""

    def get_by_natural_key(self, url, subject):
        """Lookup the object by the natural key."""
        return self.get(url=url, subject=subject)


class Patch(EMOM('patch'), models.Model):
    """Model for Patch."""

    url = models.CharField(max_length=500)
    subject = models.CharField(max_length=200)
    pipelines = models.ManyToManyField(pipeline_models.Pipeline, related_name='patches')

    objects = PatchManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.url}'

    def natural_key(self):
        """Return the natural key."""
        return (self.url, self.subject)

    class Meta:
        """Metadata."""

        ordering = ('id',)
        unique_together = ('url', 'subject')

    @property
    def gui_url(self):
        """Return a user-usable URL."""
        return self.url

    @classmethod
    def create_from_urls(cls, urls):
        """Create Patch instances from list of urls."""
        return [
            cls.objects.get_or_create(**patch)[0]
            for patch in parse_patches_from_urls(urls)
        ]
