# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.
"""Issue models file."""

from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import models as dw_models


class IssueKind(EMOM('issue_kind'), models.Model):
    """Model for IssueKind."""

    description = models.CharField(max_length=200)
    tag = models.CharField(max_length=20)
    color = models.CharField(max_length=7, default='#dc3545')
    kernel_code_related = models.BooleanField(default=False)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.description}'


class Issue(EMOM('issue'), models.Model):
    """Model for Issue."""

    kind = models.ForeignKey(IssueKind, on_delete=models.CASCADE, null=True)
    description = models.CharField(max_length=200)
    ticket_url = models.URLField(unique=True)
    resolved = models.BooleanField(default=False)
    generic = models.BooleanField(default=False)
    origin_tree = models.ForeignKey('GitTree', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.description}'

    class Meta:
        """Meta."""

        ordering = ('-id', )

    @property
    def revisions(self):
        """Return all revisions linked through occurrences."""
        rev_iids = set(
            list(
                dw_models.KCIDBTest.objects.filter(issues=self)
                .values_list('build__revision__iid', flat=True)
            ) +
            list(
                dw_models.KCIDBBuild.objects.filter(issues=self)
                .values_list('revision__iid', flat=True)
            ) +
            list(
                dw_models.KCIDBRevision.objects.filter(issues=self)
                .values_list('iid', flat=True)
            )
        )

        revisions = (
            dw_models.KCIDBRevision.objects
            .filter(iid__in=rev_iids)
            .prefetch_related(
                'gitlabjob_set',
                'gitlabjob_set__pipeline',
                'kcidbbuild_set',
                'kcidbbuild_set__kcidbtest_set',
            ).select_related(
                'tree'
            )
        )

        return revisions


class IssueRegex(EMOM('issue_regex'), models.Model):
    """Model for IssueRegex."""

    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, related_name='issue_regexes')
    text_match = models.TextField()
    file_name_match = models.CharField(max_length=200, null=True, default=None)
    test_name_match = models.CharField(max_length=200, null=True, default=None)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.test_name_match} - {self.file_name_match} - {self.text_match}'

    class Meta:
        """Meta."""

        ordering = ('-id', )
