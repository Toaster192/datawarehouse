# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to messages."""

from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM


class MessagePending(EMOM('message_pending'), models.Model):
    """Model for MessagePending."""

    body = models.TextField()
    exchange = models.CharField(max_length=50)

    def __str__(self):
        """Return __str__ formatted."""
        return self.body

    class Meta:
        """Meta."""

        ordering = ('id', )
