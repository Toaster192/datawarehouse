"""Test the scripts.misc module."""
import datetime
from unittest import mock

from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse import scripts
from datawarehouse.api.kcidb import serializers
from tests import utils


class ScriptsMiscTest(utils.TestCase):
    """Unit tests for the scripts.misc module."""

    @staticmethod
    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_objects_for_retriage(send_notif):
        """Test send_kcidb_object_for_retriage."""
        def get_date(days_ago):
            """Get timestamp from {days_ago}."""
            return timezone.now() - datetime.timedelta(days=days_ago)

        origin = models.KCIDBOrigin.objects.create(name='redhat')

        revision_1 = models.KCIDBRevision.objects.create(
            origin=origin, id='rev1',
            discovery_time=get_date(2),
            valid=False
        )
        models.KCIDBRevision.objects.create(
            origin=origin, id='rev2',
            discovery_time=get_date(2),
            valid=True,
        )
        models.KCIDBRevision.objects.create(
            origin=origin, id='rev3',
            discovery_time=get_date(4),
            valid=False,
        )
        build_1 = models.KCIDBBuild.objects.create(
            origin=origin, revision=revision_1, id='redhat:build-1',
            valid=False, start_time=get_date(2)
        )
        models.KCIDBBuild.objects.create(
            origin=origin, revision=revision_1, id='redhat:build-2',
            valid=True, start_time=get_date(2)
        )
        models.KCIDBBuild.objects.create(
            origin=origin, revision=revision_1, id='redhat:build-3',
            valid=False, start_time=get_date(4)
        )
        test_1 = models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-1',
            status=models.ResultEnum.FAIL,
            start_time=get_date(2)
        )
        models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-2',
            status=models.ResultEnum.PASS,
            start_time=get_date(2)
        )
        models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-3',
            status=models.ResultEnum.FAIL,
            start_time=get_date(4)
        )

        scripts.send_kcidb_object_for_retriage(3)
        send_notif.assert_has_calls(
            [
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'revision',
                    'object': serializers.KCIDBRevisionSerializer(revision_1).data,
                    'id': revision_1.id,
                    'iid': revision_1.iid,
                }]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'build',
                    'object': serializers.KCIDBBuildSerializer(build_1).data,
                    'id': build_1.id,
                    'iid': build_1.iid,
                }]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'test',
                    'object': serializers.KCIDBTestSerializer(test_1).data,
                    'id': test_1.id,
                    'iid': test_1.iid,
                }]),
            ]
        )
