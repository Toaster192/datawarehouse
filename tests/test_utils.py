"""Test the utils module."""

import datetime
import os
import time
from unittest import mock

import dateutil
from django import test
from django.http.request import QueryDict
from django.utils.timezone import make_aware
import responses

from datawarehouse import utils


class UtilsTestCase(test.TestCase):
    """Unit tests for the utils module."""

    TESTS_DIR = os.path.dirname(os.path.abspath(__file__))

    @responses.activate
    def test_parse_patches_from_urls(self):
        """Test the extraction of the patch subject."""
        patch_urls = ['https://server/patch1', 'https://server/patch2']
        subjects = ['panic: ensure preemption is disabled during panic()',
                    'USB: rio500: Remove Rio 500 kernel driver']

        def response_callback(resp):
            """Delay the first GET request to modify the responses order."""
            if resp.url.endswith('patch1'):
                time.sleep(0.1)
            return resp

        with responses.RequestsMock(response_callback=response_callback) as r_mock:
            r_mock.add(responses.GET, url=patch_urls[0],
                       body='From 20bb759a66be52cf4a9ddd17fddaf509e11490cd Mon Sep 17 00:00:00 2001\n' +
                       'From: Somebody <somebody@someplace.org>\n' +
                       'Date: Sun, 6 Oct 2019 17:58:00 -0700\n' +
                       f'Subject: {subjects[0]}\n' +
                       '\n' +
                       'From: Somebody <somebody@someplace.org>\n' +
                       '\n' +
                       'commit 20bb759a66be52cf4a9ddd17fddaf509e11490cd upstream.\n')
            r_mock.add(responses.GET, url=patch_urls[1],
                       body='From 015664d15270a112c2371d812f03f7c579b35a73 Mon Sep 17 00:00:00 2001\n' +
                       'From: Other <other@someplace.org>\n' +
                       'Date: Mon, 23 Sep 2019 18:18:43 +0200\n' +
                       f'Subject: {subjects[1]}\n' +
                       '\n' +
                       'From: Other <other@someplace.org>\n' +
                       '\n' +
                       'commit 015664d15270a112c2371d812f03f7c579b35a73 upstream.\n')

            result = utils.parse_patches_from_urls(patch_urls)
            expected = [{'url': url, 'subject': subject} for url, subject in zip(patch_urls, subjects)]
            self.assertEqual(expected, result)

    def test_timestamp_to_datetime(self):
        """Test timestamp_to_datetime."""
        self.assertEqual(None, utils.timestamp_to_datetime(None))

        test_datetime = datetime.datetime.now()
        test_datetime_aware = make_aware(test_datetime)

        self.assertEqual(
            test_datetime_aware,
            utils.timestamp_to_datetime(str(test_datetime))
        )

        test_timestamp = '2020-03-27T12:33:24.725Z'
        test_timestamp_datetime = datetime.datetime(2020, 3, 27, 12, 33, 24, 725000, tzinfo=dateutil.tz.UTC)
        self.assertEqual(test_timestamp_datetime, utils.timestamp_to_datetime(test_timestamp))

        test_timestamp = '2020-03-27'
        test_timestamp_datetime = datetime.datetime(2020, 3, 27, 0, 0, 0, 0, tzinfo=dateutil.tz.UTC)
        self.assertEqual(test_timestamp_datetime, utils.timestamp_to_datetime(test_timestamp))

        test_timestamp = '2020/03/27'
        test_timestamp_datetime = datetime.datetime(2020, 3, 27, 0, 0, 0, 0, tzinfo=dateutil.tz.UTC)
        self.assertEqual(test_timestamp_datetime, utils.timestamp_to_datetime(test_timestamp))

        test_timestamp = '2020-03-27T12:33:24.725+3'
        test_timestamp_datetime = datetime.datetime(2020, 3, 27, 12, 33, 24, 725000,
                                                    tzinfo=dateutil.tz.tzoffset(None, 10800))
        self.assertEqual(test_timestamp_datetime, utils.timestamp_to_datetime(test_timestamp))

    @mock.patch('datawarehouse.utils.settings.RABBITMQ_CONFIGURED', True)
    def test_send_kcidb_notification(self):
        """Test send_kcidb_notification."""
        utils.MSG_QUEUE = mock.Mock()
        utils.TIMER_MSG_QUEUE_SEND = mock.Mock()
        utils.send_kcidb_notification({'something': 'important'})
        utils.MSG_QUEUE.bulk_add.assert_called_with(
            [({'something': 'important'}, 'cki.exchange.datawarehouse.kcidb')]
        )
        self.assertTrue(utils.TIMER_MSG_QUEUE_SEND.start.called)

    @mock.patch('datawarehouse.utils.settings.RABBITMQ_CONFIGURED', False)
    def test_send_kcidb_notification_disabled(self):
        """Test send_kcidb_notification."""
        utils.MSG_QUEUE = mock.Mock()
        utils.send_kcidb_notification({'something': 'important'})
        self.assertFalse(utils.MSG_QUEUE.add.called)
        self.assertFalse(utils.MSG_QUEUE.send.called)

    def test_clean_dict(self):
        """Test clean_dict helper function."""
        self.assertEqual(
            {'a': 0, 'b': 1},
            utils.clean_dict({'a': 0, 'b': 1, 'c': None})
        )

    def test_filter_revisions_view(self):
        """Test filter_revisions_view function."""
        query = QueryDict('bar=bar&filter_email=foo&filter_gittrees=git1&filter_gittrees=git2')
        request = mock.Mock(GET=query)

        revisions = mock.Mock()
        returned_revisions, filters = utils.filter_revisions_view(request, revisions)

        self.assertEqual(
            {'filter_email': 'foo', 'filter_gittrees': ['git1', 'git2']},
            filters
        )

        revisions.filter.assert_has_calls(
            [mock.call(contacts__email__icontains='foo')],
            [mock.call(tree__name__in=['git1', 'git2'])],
        )

    def test_filter_revisions_view_empty(self):
        """Test filter_revisions_view function. No filters."""
        query = QueryDict('bar=bar&foo=foo')
        request = mock.Mock(GET=query)

        revisions = mock.Mock()
        returned_revisions, filters = utils.filter_revisions_view(request, revisions)

        self.assertEqual({}, filters)
        self.assertFalse(revisions.filter.called)

    def test_filter_revisions_view_present_empty(self):
        """Test filter_revisions_view function. Filter key is present but empty."""
        query = QueryDict('bar=bar&foo=foo&filter_email=')
        request = mock.Mock(GET=query)

        revisions = mock.Mock()
        returned_revisions, filters = utils.filter_revisions_view(request, revisions)

        self.assertEqual({}, filters)
        self.assertFalse(revisions.filter.called)

    @mock.patch('datawarehouse.utils.User')
    def test_async_send_email(self, user_mock):
        """Test async_send_email calls User.email_user correctly."""
        object_mock = mock.Mock()
        get_mock = mock.Mock()
        get_mock.return_value = object_mock
        user_mock.objects.get = get_mock

        utils.async_send_email(1, 'subject', 'message')
        get_mock.assert_called_with(id=1)
        object_mock.email_user.assert_called_with(subject='subject', message='message')
