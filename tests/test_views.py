"""Test the views module."""
from unittest import mock

from datawarehouse import models
from tests import utils


class ViewsTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the views module."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/base_simple.yaml',
        'tests/fixtures/base_many_tests.yaml',
    ]

    def setUp(self):
        """Set up tests."""
        self.hosts = models.BeakerResource.objects.all().order_by('id')
        self.tests = models.Test.objects.all().order_by('id')
        self.revision = models.KCIDBRevision.objects.get()
        self.test_runs = models.KCIDBTest.objects.filter(build__revision=self.revision)

    def test_details_test(self):
        """Test all elements from a certain test details query."""
        response = self.client.get('/details/test/1')
        self.assertContextEqual(response.context, {
            'item': self.tests[0],
            'type': 'test',
            'results': models.ResultEnum,
            'result_filter': None,
            'runs': [
                {'revision': self.revision,
                 'tests': self.test_runs.filter(test=self.tests[0])},
            ],
        })

    def test_details_test_another(self):
        """Test that getting a different test works."""
        response = self.client.get('/details/test/2')
        self.assertContextEqual(response.context, {
            'item': self.tests[1],
        })

    def test_details_tests_result(self):
        """Test filtering by results."""
        response = self.client.get('/details/test/1?result=PASS')
        self.assertContextEqual(response.context, {
            'runs': [
                {'revision': self.revision,
                 'tests': self.test_runs.filter(test=self.tests[0], status=models.ResultEnum.PASS)},
            ],
        })

        response = self.client.get('/details/test/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'runs': [
                {'revision': self.revision,
                 'tests': self.test_runs.filter(test=self.tests[0], status=models.ResultEnum.FAIL)},
            ],
        })

    def test_details_tests_table(self):
        """Test returned table."""
        response = self.client.get('/details/test/2')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(kcidbtest__test__id=2).distinct()
        })

        self.assertEqual(response.context['table'][0].total_runs, 3)
        self.assertEqual(response.context['table'][0].FAIL, 1)
        self.assertEqual(response.context['table'][0].PASS, 1)
        self.assertEqual(response.context['table'][0].ERROR, 1)

    def test_details_tests_table_result(self):
        """Test returned table when filtering by result."""
        response = self.client.get('/details/test/1?result=PASS')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(
                kcidbtest__test__id=1,
                kcidbtest__status=models.ResultEnum.PASS
            ).distinct()
        })

        response = self.client.get('/details/test/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'table': self.hosts.filter(
                kcidbtest__test__id=1,
                kcidbtest__status=models.ResultEnum.FAIL
            ).distinct()
        })

    def test_details_host(self):
        """Test all elements from a certain host details query."""
        response = self.client.get('/details/host/1')
        self.assertContextEqual(response.context, {
            'item': self.hosts[0],
            'type': 'host',
            'results': models.ResultEnum,
            'result_filter': None,
            'runs': [
                {'revision': self.revision,
                 'tests': self.test_runs.filter(environment=self.hosts[0])},
            ],
        })

    def test_details_host_another(self):
        """Test that getting a different host works."""
        response = self.client.get('/details/host/2')
        self.assertContextEqual(response.context, {
            'item': self.hosts[1],
        })

    def test_details_hosts_result(self):
        """Test filtering by results."""
        response = self.client.get('/details/host/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'runs': [
                {'revision': self.revision,
                 'tests': self.test_runs.filter(environment=self.hosts[0], status=models.ResultEnum.FAIL)},
            ],
        })

        response = self.client.get('/details/host/1?result=PASS')
        self.assertContextEqual(response.context, {
            'runs': [
                {'revision': self.revision,
                 'tests': self.test_runs.filter(environment=self.hosts[0], status=models.ResultEnum.PASS)},
            ],
        })

    def test_details_hosts_table(self):
        """Test returned table."""
        response = self.client.get('/details/host/1')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(kcidbtest__environment__id=1).distinct()
        })

        self.assertEqual(response.context['table'][0].total_runs, 3)
        self.assertEqual(response.context['table'][0].FAIL, 1)
        self.assertEqual(response.context['table'][0].PASS, 1)
        self.assertEqual(response.context['table'][0].ERROR, 1)

    def test_details_hosts_table_result(self):
        """Test returned table when filtering by result."""
        response = self.client.get('/details/host/1?result=FAIL')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(
                kcidbtest__environment__id=1,
                kcidbtest__status=models.ResultEnum.FAIL
            ).distinct()
        })

        response = self.client.get('/details/host/1?result=PASS')
        self.assertContextEqual(response.context, {
            'table': self.tests.filter(
                kcidbtest__environment__id=1,
                kcidbtest__status=models.ResultEnum.PASS
            ).distinct()
        })

    def test_issue_new(self):
        """Test creating new issues."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id})
        response = self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Check that it was created ok.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertEqual('foo bar', issue.description)
        self.assertEqual(issue_kind, issue.kind)

        # Same ticket_url. Fail.
        post_args = ('/issue', {'description': 'bar bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id})
        response = self.assert_authenticated_post(400, 'add_issue', *post_args)
        self.assertEqual(b'Issue already exists with ticket URL http://some.url', response.content)

        # Another ticket_url. Ok
        post_args = ('/issue', {'description': 'bar bar', 'ticket_url': 'http://other.url',
                                'kind_id': issue_kind.id})
        response = self.assert_authenticated_post(302, 'add_issue', *post_args)

    def test_issue_whitespace_removal(self):
        """Test issues handle trailing spaces on description and ticket_url."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        post_args = ('/issue', {'description': '   foo bar ', 'ticket_url': ' http://some.url ',
                                'kind_id': issue_kind.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertEqual(issue.description, 'foo bar')
        self.assertEqual(issue.ticket_url, 'http://some.url')

        # Change the values.
        post_args[1]['issue_id'] = issue.id
        post_args[1]['description'] = ' foo foo    '
        post_args[1]['ticket_url'] = ' http://other.url '
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertEqual(issue.description, 'foo foo')
        self.assertEqual(issue.ticket_url, 'http://other.url')

    def test_issue_new_kernel_bug_origin_tree(self):
        """Test creating new kernel bug issues, with an origin tree."""
        issue_kind = models.IssueKind.objects.create(description="Kernel bug", tag="kb", kernel_code_related=True)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Check that the origin tree is correct
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertEqual(git_tree.id, issue.origin_tree.id)

    def test_issue_new_non_kernel_bug_should_ignore_origin_tree(self):
        """Test creating new kernel bug issues, with an origin tree."""
        issue_kind = models.IssueKind.objects.create(description="Kernel bug", tag="kb", kernel_code_related=False)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Check that the origin tree is correct
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        self.assertIsNone(issue.origin_tree)

    def test_issue_edit(self):
        """Test editing issues."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue_kind_2 = models.IssueKind.objects.create(description="fail 2", tag="2")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Change the values.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        post_args = ('/issue', {'issue_id': issue.id, 'description': 'var var',
                                'ticket_url': 'http://other.url', 'kind_id': issue_kind_2.id})
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertEqual(issue.description, 'var var')
        self.assertEqual(issue.ticket_url, 'http://other.url')
        self.assertEqual(issue.kind, issue_kind_2)

    def test_issue_edit_can_add_origin_tree_on_kernel_bugs(self):
        """Test editing issues."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1", kernel_code_related=True)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Change the values.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        post_args = ('/issue', {'issue_id': issue.id,
                                'description': 'foo bar',
                                'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id})
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertEqual(git_tree.id, issue.origin_tree.id)

    def test_issue_edit_origin_tree_on_non_kernel_bugs_should_be_ignored(self):
        """Test editing issues."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1", kernel_code_related=False)
        git_tree = models.GitTree.objects.create(name="mainline")
        post_args = ('/issue', {'description': 'foo bar', 'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id})
        self.assert_authenticated_post(302, 'add_issue', *post_args)

        # Change the values.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        post_args = ('/issue', {'issue_id': issue.id, 'description': 'foo bar',
                                'ticket_url': 'http://some.url',
                                'kind_id': issue_kind.id,
                                'origin_tree_id': git_tree.id})
        self.assert_authenticated_post(302, 'change_issue', *post_args)

        # Check new values.
        issue = models.Issue.objects.get(id=issue.id)
        self.assertIsNone(issue.origin_tree)

    def test_issue_resolve(self):
        """Test resolving issues."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', kind=issue_kind, ticket_url='http://some.url')

        self.assert_authenticated_post(302, 'change_issue', f'/issue/{issue.id}/resolve')
        issue = models.Issue.objects.get(id=issue.id)
        self.assertTrue(issue.resolved)

        self.assert_authenticated_post(302, 'change_issue', f'/issue/{issue.id}/resolve')
        issue = models.Issue.objects.get(id=issue.id)
        self.assertFalse(issue.resolved)

    def test_issue_edit_url_check(self):
        """Test when editing issues it's not possible to duplicate the URL."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        self.assert_authenticated_post(
            302, 'add_issue', '/issue', {'description': 'foo bar',
                                         'ticket_url': 'http://some.url', 'kind_id': issue_kind.id})

        self.assert_authenticated_post(
            302, 'add_issue', '/issue', {'description': 'foo bar', 'ticket_url': 'http://other.url',
                                         'kind_id': issue_kind.id})

        # Change the values. The url is already used by the second issue.
        issue = models.Issue.objects.get(ticket_url='http://some.url')
        response = self.assert_authenticated_post(
            400, 'change_issue', '/issue', {'issue_id': issue.id, 'description': 'foo bar',
                                            'ticket_url': 'http://other.url', 'kind_id': issue_kind.id})
        self.assertEqual(b'Issue already exists with ticket URL http://other.url', response.content)


class TestIssueRegex(utils.TestCase):
    """Issue Regex test case."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml'
    ]

    def setUp(self):
        self.issue = models.Issue.objects.get(id=1)

    def test_create(self):
        """Test create IssueRegex."""
        post_args = (
            '/issue/regex',
            {
                'action': 'new',
                'issue_id_select': self.issue.id,
                'text_match': ' foo',
                'file_name_match': ' bar',
                'test_name_match': ' foobar ',
            }
        )
        self.assert_authenticated_post(302, 'add_issueregex', *post_args)

        issue_regex = models.IssueRegex.objects.get(issue=self.issue)
        self.assertEqual('foo', issue_regex.text_match)
        self.assertEqual('bar', issue_regex.file_name_match)
        self.assertEqual('foobar', issue_regex.test_name_match)
        self.assertEqual(self.issue, issue_regex.issue)

    def test_create_incomplete(self):
        """Test create IssueRegex without all the fields."""
        post_args = (
            '/issue/regex',
            {
                'action': 'new',
                'issue_id_select': self.issue.id,
                'text_match': ' foo',
            }
        )
        self.assert_authenticated_post(302, 'add_issueregex', *post_args)

        issue_regex = models.IssueRegex.objects.get(issue=self.issue)
        self.assertEqual('foo', issue_regex.text_match)
        self.assertEqual(None, issue_regex.file_name_match)
        self.assertEqual(None, issue_regex.test_name_match)
        self.assertEqual(self.issue, issue_regex.issue)

    @mock.patch('datawarehouse.views.post_save')
    def test_edit(self, post_save):
        """Test edit IssueRegex."""
        issue_regex = models.IssueRegex.objects.create(
            issue=self.issue,
            text_match='foo',
        )
        issue_2 = models.Issue.objects.create(
            description='foo foo',
            kind=models.IssueKind.objects.create(description="fail 2", tag="2"),
            ticket_url='http://some.other.url'
        )

        post_args = (
            '/issue/regex',
            {
                'action': 'edit',
                'issue_regex_id': issue_regex.id,
                'issue_id_select': issue_2.id,
                'text_match': 'barbar ',
                'file_name_match': ' bar',
                'test_name_match': ' foobar',
            }
        )
        self.assert_authenticated_post(302, 'change_issueregex', *post_args)

        issue_regex = models.IssueRegex.objects.get(id=issue_regex.id)
        self.assertEqual('barbar', issue_regex.text_match)
        self.assertEqual('bar', issue_regex.file_name_match)
        self.assertEqual('foobar', issue_regex.test_name_match)
        self.assertEqual(issue_2, issue_regex.issue)

        self.assertTrue(post_save.send.called)
        post_save.send.assert_called_with(models.IssueRegex)

    @mock.patch('datawarehouse.views.post_save')
    def test_edit_incomplete(self, post_save):
        """Test edit IssueRegex."""
        issue_regex = models.IssueRegex.objects.create(
            issue=self.issue,
            text_match='foo',
        )

        post_args = (
            '/issue/regex',
            {
                'action': 'edit',
                'issue_regex_id': issue_regex.id,
                'issue_id_select': self.issue.id,
                'text_match': 'barbar ',
            }
        )
        self.assert_authenticated_post(302, 'change_issueregex', *post_args)

        issue_regex = models.IssueRegex.objects.get(id=issue_regex.id)
        self.assertEqual('barbar', issue_regex.text_match)
        self.assertEqual(None, issue_regex.file_name_match)
        self.assertEqual(None, issue_regex.test_name_match)

    def test_delete(self):
        """Test delete IssueRegex."""
        issue_regex = models.IssueRegex.objects.create(
            issue=self.issue,
            text_match='foo',
        )
        post_args = (
            '/issue/regex',
            {
                'action': 'delete',
                'issue_regex_id': issue_regex.id,
            }
        )
        self.assert_authenticated_post(302, 'delete_issueregex', *post_args)
        self.assertFalse(
            models.IssueRegex.objects.filter(id=issue_regex.id).exists()
        )
