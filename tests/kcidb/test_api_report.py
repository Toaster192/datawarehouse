"""Test the report api module."""
import datetime
import json
import os
from pathlib import Path

from django.utils import timezone
from pytz import UTC

from datawarehouse import models
from datawarehouse.api.kcidb.serializers import KCIDBRevisionSerializer
from datawarehouse.serializers import ReportSerializer
from tests import utils


class APIReportTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the report endpoints."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @staticmethod
    def load_email(email):
        """Load email from assets."""
        file_name = os.path.join(utils.ASSETS_DIR, email)
        file_content = Path(file_name).read_text()

        return {
            'content': json.loads(file_content)
        }

    def test_report_create(self):
        """Test creating a report."""
        revision = models.KCIDBRevision.objects.first()
        email = self.load_email('email_complete')

        self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/kcidb/revisions/{revision.iid}/reports',
            json.dumps(email), content_type="application/json")

        report = revision.reports.first()

        self.assertEqual(
            '\nHello,\n\nWe ran automated tests on a patchset that was proposed for merging into this\nkernel tree.',
            report.body)
        self.assertEqual(
            '=?utf-8?b?4pyF?= PASS: Re: [RHEL    PATCH 000000000 2/2] ' +
            'tpm: Revert\n\t"Lorem ipsum dolor sit amet, consectetur adipisci\'s"',
            report.subject)
        self.assertEqual(
            datetime.datetime(2020, 1, 9, 19, 33, 16, tzinfo=UTC),
            report.sent_at
        )
        self.assertEqual(
            '<cki.0.5FM2O05IQP@redhat.com>',
            report.msgid)

        report.addr_to.get(email='redacted-email-add@redhat.com')
        report.addr_to.get(email='redacted-email@redhat.com')
        report.addr_cc.get(email='test@redhat.com')

    def test_report_create_duplicated(self):
        """Test creating a report two times."""
        revision = models.KCIDBRevision.objects.first()
        email = self.load_email('email_complete')

        # First time created.
        response = self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/kcidb/revisions/{revision.iid}/reports',
            json.dumps(email), content_type="application/json")
        self.assertEqual(1, revision.reports.count())

        self.assertEqual(
            ReportSerializer(revision.reports.first()).data,
            response.json()
        )

        # Second time not created.
        response = self.assert_authenticated_post(
            400, 'add_report',
            f'/api/1/kcidb/revisions/{revision.iid}/reports',
            json.dumps(email), content_type="application/json")
        self.assertEqual(b'"MsgID already exists."', response.content)
        self.assertEqual(1, revision.reports.count())

    def test_report_list(self):
        """Test getting revision's reports."""
        revision = models.KCIDBRevision.objects.first()
        revision.reports.create(
            body='content', subject='subject', sent_at=timezone.now(), msgid='<someting@redhat.com>',
        )
        revision.reports.create(
            body='content', subject='subject', sent_at=timezone.now(), msgid='<someting_2@redhat.com>',
        )

        response = self.client.get(f'/api/1/kcidb/revisions/{revision.iid}/reports')

        self.assertEqual(
            response.json()['results'],
            ReportSerializer(revision.reports.all(), many=True).data,
        )

    def test_report_get(self):
        """Test getting revision's reports."""
        revision = models.KCIDBRevision.objects.first()
        report = revision.reports.create(
            body='content',
            subject='subject',
            sent_at=timezone.now(),
            msgid='<someting@redhat.com>',
        )

        response = self.client.get(f'/api/1/kcidb/revisions/{revision.iid}/reports/{report.id}')

        self.assertEqual(
            response.json(),
            ReportSerializer(revision.reports.get(id=report.id)).data,
        )

    def test_report_get_by_msgid(self):
        """Test getting revision's reports by msgid"""
        revision = models.KCIDBRevision.objects.first()
        report = revision.reports.create(
            body='content',
            subject='subject',
            sent_at=timezone.now(),
            msgid='<someting@redhat.com>',
        )

        response = self.client.get(f'/api/1/kcidb/revisions/{revision.iid}/reports/{report.msgid}')

        self.assertEqual(
            response.json(),
            ReportSerializer(revision.reports.get(id=report.id)).data,
        )

    def test_report_create_emails_empty(self):
        """Test creating a report with empty email fields."""
        revision = models.KCIDBRevision.objects.first()
        email = self.load_email('email_no_recipients')

        self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/kcidb/revisions/{revision.iid}/reports',
            json.dumps(email), content_type="application/json")

        report = revision.reports.first()

        self.assertEqual(0, report.addr_to.count())
        self.assertEqual(0, report.addr_cc.count())

    def test_create_encoded(self):
        """Test creating report with body encoded."""
        revision = models.KCIDBRevision.objects.first()
        email = self.load_email('email_b64_encoded')

        self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/kcidb/revisions/{revision.iid}/reports',
            json.dumps(email), content_type="application/json")

        report = revision.reports.first()

        self.assertEqual(
            '\nHello,\n\nW',
            report.body[:10])

    def test_missing_report(self):
        """Test getting missing reports."""
        revision = models.KCIDBRevision.objects.first()
        revision.reports.create(
            body='content', subject='subject', sent_at=timezone.now(), msgid='<someting@redhat.com>',
        )
        response = self.client.get('/api/1/kcidb/revisions/reports/missing')
        self.assertEqual(
            response.json()['results'],
            KCIDBRevisionSerializer(
                models.KCIDBRevision.objects.exclude(id=revision.id),
                many=True
            ).data
        )

    def test_missing_report_since(self):
        """Test getting missing reports with since filter."""
        since = datetime.datetime.now() - datetime.timedelta(hours=48)

        models.KCIDBRevision.objects.all().update(
            # Make them all older than one day
            discovery_time=timezone.now() - datetime.timedelta(hours=25)
        )
        models.KCIDBRevision.objects.filter(
            id='public_revision_invalid'
        ).update(
            # Move it outside the 'since' filter
            discovery_time=timezone.now() - datetime.timedelta(hours=48+1)
        )

        response = self.client.get(f'/api/1/kcidb/revisions/reports/missing?since={since}')

        self.assertEqual(
            response.json()['results'],
            KCIDBRevisionSerializer(
                models.KCIDBRevision.objects.exclude(id='public_revision_invalid'),
                many=True
            ).data
        )

    def test_missing_report_since_with_report(self):
        """Test getting missing reports with since filter."""
        since = datetime.datetime.now() - datetime.timedelta(hours=48)

        models.KCIDBRevision.objects.all().update(
            # Make them all older than one day
            discovery_time=timezone.now() - datetime.timedelta(hours=25)
        )
        revision = models.KCIDBRevision.objects.first()
        revision.reports.create(
            body='content', subject='subject', sent_at=timezone.now(), msgid='<someting@redhat.com>',
        )

        response = self.client.get(f'/api/1/kcidb/revisions/reports/missing?since={since}')
        self.assertEqual(
            response.json()['results'],
            KCIDBRevisionSerializer(
                models.KCIDBRevision.objects.exclude(id=revision.id),
                many=True
            ).data
        )

    def test_report_multipart(self):
        """Test creating report from multipart email."""
        revision = models.KCIDBRevision.objects.first()
        email = self.load_email('email_multipart')

        self.assert_authenticated_post(
            201, 'add_report',
            f'/api/1/kcidb/revisions/{revision.iid}/reports',
            json.dumps(email), content_type="application/json")

        report = revision.reports.first()

        self.assertEqual(
            '\nHello,\n\nW',
            report.body[:10])
