"""TestCase with utils."""
import json
import os

from django import test
from django.contrib.auth.models import Permission  # pylint: disable=imported-auth-user
from django.contrib.auth.models import User
from django.core import serializers
from django.db.models.query import QuerySet
from django.test import Client

ASSETS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')


class TestCase(test.TestCase):
    # pylint: disable=invalid-name
    """TestCase class."""

    def __init__(self, *args, **kwargs):
        """Override Init."""
        super(TestCase, self).__init__(*args, **kwargs)

    def _resolve_querysets(self, data):
        """Resolve QuerySets."""
        if isinstance(data, dict):
            for key, value in data.items():
                data[key] = self._resolve_querysets(value)
        elif isinstance(data, list):
            for key, value in enumerate(data):
                data[key] = self._resolve_querysets(value)
        elif isinstance(data, QuerySet):
            data = list(data)
        elif hasattr(data, 'object_list'):
            data = self._resolve_querysets(data.object_list)

        return data

    def assertContextEqual(self, first, second):
        """
        Assert Context items are equal.

        Second can contain only some of the keys from first.
        """
        for key in second.keys():
            self.assertEqual(self._resolve_querysets(first[key]), self._resolve_querysets(second[key]), key)

    @staticmethod
    def insertObjects(objects):
        """
        Insert objects into the database.

        Objects are formatted as returned by dumpdata --format json.
        """
        result = []
        for m in serializers.deserialize('json', json.dumps(list(objects))):
            m.save()
            result.append(m.object)
        return result

    @staticmethod
    def get_test_user():
        """Get a test user object."""
        try:
            return User.objects.get(username='test')
        except User.DoesNotExist:
            return User.objects.create_user(username='test')

    @staticmethod
    def get_authenticated_client(user):
        """Create and return an authenticated client instance."""
        client = Client()
        client.force_login(user=user)

        return client

    def assert_authenticated_post(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated POST."""
        return self.assert_authenticated('post', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated_delete(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated DELETE."""
        return self.assert_authenticated('delete', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated_put(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated PUT."""
        return self.assert_authenticated('put', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated(self, method, response_code, permission_codenames, *args,
                             user=None, redirect_login=False, redirect_url=None, **kwargs):
        """
        Test an authenticated endpoint.

        Ensure the request is forbidden without the correct permissions.
        """
        user = user or self.get_test_user()
        auth_client = self.get_authenticated_client(user)

        if isinstance(permission_codenames, str):
            permission_codenames = [permission_codenames]

        permissions = Permission.objects.filter(codename__in=permission_codenames)

        method_anon = getattr(self.client, method)
        method_auth = getattr(auth_client, method)

        # Test anonymous request.
        response = method_anon(*args, **kwargs)
        if redirect_login:
            self.assertRedirects(response, redirect_url)
        else:
            self.assertIn(response.status_code, [400, 401, 403])

        if permission_codenames:
            # Authenticated request without permission.
            user.user_permissions.remove(*permissions)
            response = method_auth(*args, **kwargs)
            self.assertIn(response.status_code, [400, 401, 403])

        # Authenticated request with permission.
        user.user_permissions.add(*permissions)
        response = method_auth(*args, **kwargs)
        self.assertEqual(response_code, response.status_code)
        user.user_permissions.remove(*permissions)

        return response

    def assertQuerySetEqual(self, first, second, description):
        """Compare two querysets."""
        self.assertEqual(set(first), set(second), description)
