"""Test cron.py."""
import datetime

from django.utils import timezone

from datawarehouse import cron
from datawarehouse import models
from tests import utils


class TestDeleteArtifacts(utils.TestCase):
    """Test cron.delete_expired_artifacts method."""

    @staticmethod
    def _expiry_in(valid_for):
        """Calculate expiry date."""
        return timezone.now() + datetime.timedelta(days=valid_for)

    def test_delete(self):
        """Test delete_expired_artifacts deletes expired artifacts."""
        models.Artifact.objects.bulk_create([
            # Expired
            models.Artifact(name='test_1', url='http://test_1', valid_for=1, expiry_date=self._expiry_in(-2)),
            models.Artifact(name='test_2', url='http://test_2', valid_for=1, expiry_date=self._expiry_in(-1)),
            # Still valid
            models.Artifact(name='test_3', url='http://test_3', valid_for=1, expiry_date=self._expiry_in(1)),
            models.Artifact(name='test_4', url='http://test_4', valid_for=1, expiry_date=self._expiry_in(2)),
        ])

        self.assertEqual(4, models.Artifact.objects.count())
        cron.delete_expired_artifacts()
        self.assertEqual(2, models.Artifact.objects.count())

        # These would raise if not present.
        models.Artifact.objects.get(name='test_3')
        models.Artifact.objects.get(name='test_4')
