# Pipeline

## Get

Retrieve information about a pipeline.

`GET /api/1/pipeline/$pipeline_id`

Example of response:

```json
{
    "commit_id": "e3cf31e074f3020a734149ea6edc426c0d1ffea5",
    "commit_message_title": "[redhat] kernel-3.10.0-1121.el7",
    "project": {
        "project_id": 2,
        "path": "cki-project/cki-pipeline"
    },
    "gittree": {
        "name": "rhel7"
    },
    "web_url": "https://xci32.lab.eng.rdu2.redhat.com/cki-project/cki-pipeline/pipelines/370445",
    "pipeline_id": 370445,
    "created_at": "2020-01-07T10:31:18.565000Z",
    "started_at": "2020-01-07T10:31:21.223000Z",
    "finished_at": "2020-01-07T10:53:19.181000Z",
    "duration": 1312,
    "kernel_type": "redhat",
    "test_hash": "7af422a002efc62a4461a93a2d0a65ac0f15386c",
    "tag": "-1121.el7",
    "make_target": "rpm",
    "kernel_version": "3.10.0-1121.el7.cki",
    "trigger_variables": {},
}
```
