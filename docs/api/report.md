# Report

A report represents a email sent by the reporter.

## Create

Create a Report on certain KCIDB Revision

`POST /api/1/kcidb/revisions/$revision_id/reports`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `revision_id` | `str/int` | Yes | `id` or `iid` of the revision. |
| `content` | `mbox` | Yes | Mbox of the email. |

## Get by KCIDB Revision and ID

Get a certain report from a revision.

`GET /api/1/kcidb/revisions/$revision_id/reports/$report_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `revision_id` | `str/int` | Yes | `id` or `iid` of the revision. |
| `report_id` | `str/int` | Yes | `id` or `msgid` of the report. |

Example of response:

```json
{
    "id": 7,
    "addr_to": [
        {
            "email": "email@redhat.com"
        },
        {
            "email": "another@redhat.com"
        }
    ],
    "addr_cc": [],
    "addr_bcc": [],
    "subject": "✅ PASS: Re: [PATCH] lorem ipsum",
    "body": "Email body.",
    "sent_at": "2020-01-14T18:37:23Z",
    "pipeline": 16898
}
```

## List by KCIDB Revision

Get a list of reports for a revision.

`GET /api/1/kcidb/revisions/$revision_id/reports`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `revision_id` | `str/int` | Yes | `id` or `iid` of the revision. |

Example of response:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 7,
            "addr_to": [
                {
                    "email": "email@redhat.com"
                },
                {
                    "email": "another@redhat.com"
                }
            ],
            "addr_cc": [],
            "addr_bcc": [],
            "subject": "✅ PASS: Re: [PATCH] lorem ipsum",
            "body": "Email body.",
            "sent_at": "2020-01-14T18:37:23Z",
            "pipeline": 16898
        }
    ]
}
```

## Get Missing

Get a list of KCIDB Revisions without a report.

Note: Revisions that finished less than 24 hours before the request
are not exposed on this endpoint to wait for them to finish.

`GET /api/1/kcidb/revisions/reports/missing`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `since` | `datetime` | No | Get pipelines newer that this date.|

Example of response:

```json
{
    "count": 1242,
    "next": "http://server/api/1/report/missing?limit=30&offset=30",
    "previous": null,
    "results": [
        {
            "id": "b8fba93561c984f336d47d6d544be3a2a920bac3",
            "origin": "redhat",
            "tree_name": "upstream",
            "git_repository_url": "https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git",
            "git_repository_branch": "queue/5.7",
            "git_commit_hash": "b8fba93561c984f336d47d6d544be3a2a920bac3",
            "publishing_time": "2020-06-30T17:06:26.357000Z",
            "discovery_time": "2020-06-30T17:06:26.357000Z",
            "valid": true,
            "misc": {
                "iid": 137
            }
        }
    ]
}
```
