# Feature flags

Some features are enabled or disabled by setting feature flags on the
deployment environment.

These configurations allow customization of certain behaviours of the
Datawarehouse.

<!-- markdownlint-disable line-length -->
| Variable Name              | Default | Description                                   |
|----------------------------|---------|-----------------------------------------------|
| `FF_ALLOW_DEBUG_PIPELINES` | `False` | Allow submitting debug pipelines              |
| `FF_SIGNUP_ENABLED`        | `False` | Enable the creation of new accounts on the UI |
<!-- markdownlint-restore -->
